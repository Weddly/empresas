﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Controllers
{
    public class EnterprisesController : ControllerBase
    {
        private AppSettings _appSettings;
        protected ApplicationContext _context;

        public EnterprisesController(ApplicationContext context, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }

        [HttpGet]
        [Authorize]
        [Route("api/[controller]/GetAll")]
        public IActionResult GetAll()
        {
            var users = _context.Enterprises.ToList();
            return Ok(users);
        }

        [HttpGet]
        [Authorize]
        [Route("api/[controller]/{id}")]
        public IActionResult GetById(int id)
        {
            var user = _context.Enterprises.FirstOrDefault(x=>x.Id == id);
            return Ok(user);
        }
    }
}
