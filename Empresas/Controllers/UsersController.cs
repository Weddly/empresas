﻿using Empresas.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Empresas.Controllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private AppSettings _appSettings;
        protected ApplicationContext _context;

        public UsersController(ApplicationContext context, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/auth/sign_in")]
        public IActionResult Authenticate([FromBody]User userParam)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == userParam.UserName && x.Password == userParam.Password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return Ok(user);
        }

        [HttpGet]
        [Authorize]
        [Route("api/[controller]/GetAll")]
        public IActionResult GetAll()
        {
            var users = _context.Users.ToList();
            return Ok(users);
        }

        [HttpPost]
        [Route("api/[controller]/create")]
        public IActionResult Create([FromBody]User user)
        {
            _context.Users.Add(user);
            return Ok();
        }
    }
}