﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Model
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
